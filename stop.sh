#!/bin/bash

while read line; do
    kill -SIGQUIT $PPID
done < /dev/stdin
