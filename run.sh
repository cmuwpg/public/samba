#!/bin/bash

echo ${AD_JOIN_PASSWORD} | kinit ${AD_JOIN_USER}@${AD_REALM}

net ads join -U ${AD_JOIN_USER}%${AD_JOIN_PASSWORD} createcomputer="${AD_JOIN_OU}" --configfile /etc/samba/smb.conf --no-dns-updates

exec supervisord -c /etc/supervisor/supervisord.conf
