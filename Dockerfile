FROM debian:buster-slim

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get -qq update && apt-get -qq -y --no-install-recommends install \
    supervisor samba samba-vfs-modules \
    krb5-user libpam-krb5 \
    winbind libnss-winbind libpam-winbind \
    attr acl

RUN mv /etc/nsswitch.conf /etc/nsswitch.conf.orig
COPY nsswitch.conf /etc/nsswitch.conf

COPY run.sh /
COPY stop.sh /

COPY supervisord.conf /etc/supervisor/supervisord.conf

ENTRYPOINT ["/run.sh"]
